module.exports = {
  siteMetadata: {
    title: `ScaleRite IT Services`,
    description: `ScaleRite is a Managed Service Provider based out of Lawrenceville, GA that works with small to medium sized businesses to take their operations to the next level.`,
    author: `Michael Martin`,
    menuLinks: [
      {
        name: "Home",
        link: "/",
      },
      {
        name: "About Us",
        link: "/about",
      },
      {
        name: "Services",
        link: "/services",
      },
      /*{
        name: "Pricing",
        link: "/pricing",
      },
      */
      //commenting out the Pricing tab since we're not going to advertise this field.  Seeing if this push will redeploy the website to amplify
      {
        name: "Contact Us",
        link: "/contact",
      },
    ],
  },
  plugins: [
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    `gatsby-transformer-remark`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/src/images`,
        name: `images`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `markdown-pages`,
        path: `${__dirname}/src/markdown`,
      },
    },
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-sass`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/square-logo.png`, // This path is relative to the root of the site.
      },
    },

    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
