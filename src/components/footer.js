import React from "react"
import Navbar from "react-bootstrap/Navbar"
import logo from "../images/ScaleRiteLogo(dark-bg).png"
import Container from "react-bootstrap/Container"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"
import Nav from "react-bootstrap/Nav"

const Footer = ({ siteTitle, menuLinks }) => (
  <footer className="motion-bg">
    <Container className="inner">
      <Row className="text-center">
        <Col>
          <Navbar>
            <Nav>
              {menuLinks.map((link, key) => (
                <Nav.Link key={key} href={link.link}>
                  {link.name}
                </Nav.Link>
              ))}
            </Nav>
          </Navbar>
        </Col>
      </Row>
      <Row className="text-center inner">
        <img className="footer-logo" alt={siteTitle} src={logo} />
      </Row>
    </Container>
  </footer>
)

export default Footer
