import React from "react"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"
import Fade from "react-reveal/Fade"

const IconDisplay = ({ items, itemsPerRow }) => {
  let arrayGrouping = (list, rowLength) => {
    let idx = 0
    let result = []
    while (idx < list.length) {
      if (idx % rowLength === 0) result.push([])
      result[result.length - 1].push(list[idx++])
    }
    return result
  }

  let rows = arrayGrouping(items, itemsPerRow)
  return (
    <div>
      {rows.map((row, key) => (
        <Row key={key}>
          {console.log(row)}
          {row.map((col, key) => (
            <Col key={key} xs={12} sm={6} md={3}>
              <a href={col.node.frontmatter.learnMoreLink}>
                <Fade clear duration={2000} delay={key * 100}>
                  <div className="icon-box">
                    <img
                      className="service-icon"
                      src={col.node.frontmatter.icon.publicURL}
                      alt="service icon"
                    />
                    <img
                      className="service-icon-hover"
                      src={col.node.frontmatter.iconHover.publicURL}
                      alt="service icon"
                    />
                  </div>
                </Fade>
                <h4>{col.node.frontmatter.title}</h4>
              </a>
            </Col>
          ))}
        </Row>
      ))}
    </div>
  )
}

export default IconDisplay
