import PropTypes from "prop-types"
import React from "react"
import Navbar from "react-bootstrap/Navbar"
import logo from "../images/ScaleRiteLogo.png"
import Nav from "react-bootstrap/Nav"
import Container from "react-bootstrap/Container"

const Header = ({ siteTitle, menuLinks }) => (
  <Navbar className="header-nav" expand="lg">
    <Container>
      <Navbar.Brand href="/">
        <img className="navbar-logo" alt={siteTitle} src={logo} />
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ml-auto">
          <span className="phone-cta">
            CALL US: <a href="tel:6787713009">(678)771-3009</a>
          </span>
          {menuLinks.map((link, key) => (
            <Nav.Link key={key} href={link.link}>
              {link.name}
            </Nav.Link>
          ))}
        </Nav>
      </Navbar.Collapse>
    </Container>
  </Navbar>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
