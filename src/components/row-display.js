import React from "react"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"

const RowDisplay = ({ items, itemsPerRow }) => {
  let arrayGrouping = (list, rowLength) => {
    let idx = 0
    let result = []
    while (idx < list.length) {
      if (idx % rowLength === 0) result.push([])
      result[result.length - 1].push(list[idx++])
    }
    return result
  }

  let rows = arrayGrouping(items, itemsPerRow)
  return (
    <div>
      {rows.map((row, key) => (
        <Row key={key}>
          {console.log(row)}
          {row.map((col, key) => (
            <Col key={key} sm={12} md={6}>
              <a href={col.node.frontmatter.learnMoreLink}>
                <h3>{col.node.frontmatter.title}</h3>
              </a>
              <div dangerouslySetInnerHTML={{ __html: col.node.html }} />
            </Col>
          ))}
        </Row>
      ))}
    </div>
  )
}

export default RowDisplay
