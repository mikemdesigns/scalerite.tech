import React from "react"
import Row from "react-bootstrap/Row"
import Container from "react-bootstrap/Container"
import Button from "react-bootstrap/Button"
import Fade from "react-reveal/Fade"
import makeCarousel from "react-reveal/makeCarousel"

const CarouselUI = ({ children }) => (
  <div className="carouselContainer">{children}</div>
)
const Carousel = makeCarousel(CarouselUI)

const Hero = () => {
  return (
    <Row id="hero" bsPrefix="text-center" className="motion-bg">
      <div className="hero-bg-image">
        <div className="hero-overlay">
          {/* <img src={overlayImg} /> */}
          <Container className="inner">
            <div className="hero-text">
              <Carousel defaultWait={3000}>
                <Fade>
                  <h1 className="hero-title">Scale Up Your IT</h1>
                </Fade>
                <Fade>
                  <h1 className="hero-title">Scale Up Your Business</h1>
                </Fade>
              </Carousel>
              <Fade left delay={500}>
                <h2 className="hero-slogan">
                  Your IT setup should support your business goals. Our
                  solutions will get you there the right way.
                </h2>
              </Fade>

              <a href="/assessment">
                <Fade delay={2000}>
                  <Button variant="primary" size="lg" className="hero-cta">
                    <h2>See How We Can Help</h2>
                  </Button>
                </Fade>
              </a>
            </div>
          </Container>
        </div>
      </div>
    </Row>
  )
}

export default Hero
