import React from "react"
import Card from "react-bootstrap/Card"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"
import Button from "react-bootstrap/Button"
import { Link } from "gatsby"
import Fade from "react-reveal/Fade"

export const CardDisplay = ({ cards }) => (
  <Row>
    {cards.map((item, key) => (
      <Col key={key} xs={12} sm={6} lg={3} className="serv-cols">
        <Fade bottom delay={key * 100}>
          <Card className="shadowed">
            <Card.Body>
              <Card.Title>{item.node.frontmatter.title}</Card.Title>
              {item.node.frontmatter.icon.publicURL && (
                <Card.Img src={item.node.frontmatter.icon.publicURL} />
              )}
              <Card.Body dangerouslySetInnerHTML={{ __html: item.node.html }} />
              {item.node.frontmatter.learnMoreLink && (
                <Link to={item.node.frontmatter.learnMoreLink}>
                  <Button variant="primary">
                    {item.node.frontmatter.learnMoreLinkText}
                  </Button>
                </Link>
              )}
            </Card.Body>
          </Card>
        </Fade>
      </Col>
    ))}
  </Row>
)

export default CardDisplay
