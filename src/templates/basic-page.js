import React from "react"
import { graphql } from "gatsby"
import SEO from "../components/seo"
import Layout from "../components/layout"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"
import Container from "react-bootstrap/Container"
import DownloadIcon from "../images/checklist.svg"
import Fade from "react-reveal/Fade"

export default ({ data }) => {
  const post = data.markdownRemark
  return (
    <Layout>
      <SEO title={post.frontmatter.title} />
      <Row id="hero" className="motion-bg text-center">
        <Col className="hero-overlay">
          <div className="hero-text">
            <Fade>
              <h1 className="hero-title">{post.frontmatter.title}</h1>
            </Fade>
          </div>
        </Col>
      </Row>
      <Container className="content">
        <Row className="justify-content-center">
          <Col sm="12" className="main-content">
            <div dangerouslySetInnerHTML={{ __html: post.html }} />
          </Col>
        </Row>
        <Row className="justify-content-center download-cta">
          <Col>
            <p>Ready to Get Started?</p>
            <img src={DownloadIcon} alt="Download our free assessment" />

            <a href="../../ScaleRite_IT_Security_Checklist.docx">
              <h4>Download Our Free Services Checklist</h4>
            </a>
          </Col>
        </Row>
      </Container>
    </Layout>
  )
}
export const query = graphql`
  query($slug: String!) {
    markdownRemark(frontmatter: { slug: { eq: $slug } }) {
      html
      frontmatter {
        title
      }
    }
  }
`
