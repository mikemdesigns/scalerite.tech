import React from "react"
import SEO from "../components/seo"
import Layout from "../components/layout"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"
import Container from "react-bootstrap/Container"

export default NotFoundPage => (
  <Layout>
    <SEO title="Not Found" />
    <Row id="hero" className="motion-bg text-center">
      <Col>
        <div className="hero-text">
          <h1 className="hero-title">Hmmm...Something's Off</h1>
        </div>
      </Col>
    </Row>
    <Container className="content">
      <Row>
        <Col sm="12" className="main-content">
          <p>
            <b>
              Sorry, it looks like the page you're looking for isn't here.
              Please check the url and try again
            </b>
          </p>
        </Col>
        <Col className="sidebar"></Col>
      </Row>
    </Container>
  </Layout>
)
