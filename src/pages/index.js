import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Hero from "../components/hero"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"
import Container from "react-bootstrap/Container"
import CardDisplay from "../components/card_display"
import Jumbotron from "react-bootstrap/Jumbotron"
import Button from "react-bootstrap/Button"
import IconDisplay from "../components/icon-display"
import Fade from "react-reveal/Fade"
import { graphql } from "gatsby"
import dell from "../images/dell-logo.png"

const IndexPage = ({ data }) => {
  return (
    <Layout>
      <SEO title="Home" />
      <Hero />
      <Container>
        <Row className="intro-section">
          <Col>
            <Jumbotron className="shadowed">
              <Row>
                <Col sm={12} md={6} className="jumbo-left">
                  <span>
                    Your IT setup should support your business goals. Our
                    solutions will get you there the right way.
                  </span>
                </Col>
                <Col sm={12} md={6} className="jumbo-right">
                  <p>
                    At ScaleRite, we focus on small to medium businesses and
                    provide the IT Services they need to take their business to
                    the next level.
                  </p>
                  <p>
                    Our office is located in Lawrenceville, GA. We currently
                    specialize in servicing small to mid-size businesses. 
                  </p>
                </Col>
              </Row>
            </Jumbotron>
          </Col>
        </Row>
      </Container>
      <Row className="offerings motion-bg text-center">
        <Container>
          <section className="services motion-bg">
            <h2 className="section-title light">What We'll Do For You</h2>
            <CardDisplay cards={data.overview.edges} />
          </section>
        </Container>
      </Row>
      <Row>
        <Container>
          <section className="services-grid text-center">
            <h2 className="section-title">How We Do It</h2>
            <IconDisplay items={data.services.edges} itemsPerRow="4" />
            <Row className="services-cta">
              <Col>
                <Fade>
                  <h4>Want to know how we can improve your business?</h4>
                </Fade>
                <Fade bottom>
                  <Button>
                    <a href="/assessment/">
                      <h4>Download Our Free Technology Assessment</h4>
                    </a>
                  </Button>
                </Fade>
              </Col>
            </Row>
          </section>
        </Container>
      </Row>
      <Row className="business-partners text-center">
        <Container>
          <section>
            <h2 className="section-title">Business Partners</h2>
            <Row>
              <Col>
                <Fade>
                  <p className="text-center">
                    We are proudly partnering with the following providers
                  </p>
                  <Row className="partner-icons">
                    <Col className="text-center">
                      <img
                        src={dell}
                        width="100px"
                        height="auto"
                        alt="dell logo"
                      />
                    </Col>
                  </Row>
                </Fade>
              </Col>
            </Row>
          </section>
        </Container>
      </Row>
    </Layout>
  )
}

export const query = graphql`
  query {
    overview: allMarkdownRemark(
      filter: { fileAbsolutePath: { glob: "**/overview/**" } }
    ) {
      edges {
        node {
          frontmatter {
            title
            icon {
              publicURL
            }
          }
          html
        }
      }
    }
    services: allMarkdownRemark(
      filter: { fileAbsolutePath: { glob: "**/details/**" } }
    ) {
      edges {
        node {
          frontmatter {
            title
            learnMoreLink
            learnMoreLinkText
            icon {
              publicURL
            }
            iconHover {
              publicURL
            }
          }
          html
        }
      }
    }
  }
`

export default IndexPage
