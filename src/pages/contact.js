import React from "react"
import SEO from "../components/seo"
import Layout from "../components/layout"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"
import Container from "react-bootstrap/Container"
import Form from "react-bootstrap/Form"
import Button from "react-bootstrap/Button"
import Fade from "react-reveal/Fade"

export default NotFoundPage => (
  <Layout>
    <SEO title="Contact" />
    <Row id="hero" className="motion-bg text-center">
      <Col className="hero-overlay">
        <div className="hero-text">
          <Fade>
            <h1 className="hero-title">Contact Us</h1>
          </Fade>
        </div>
      </Col>
    </Row>
    <Container className="content">
      <Row className="justify-content-center">
        <Col className="first-sidebar" sm="12" md="4">
          <h4>Jeremy Bowden</h4>
          <p>Owner </p>
          <p>
            <span>Phone: (678)771-3009</span>
          </p>
        </Col>
        <Col className="main-content" sm="12" md="8">
          <p>If you have any questions, we would love to hear from you!</p>
          <Form
            action="https://getform.io/f/6683a7ec-5c7e-4c03-8490-62529a4452e8"
            method="POST"
          >
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control type="email" name="email" />
              <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text>
            </Form.Group>

            <Form.Group controlId="formFullName">
              <Form.Label>Full Name</Form.Label>
              <Form.Control type="text" name="name" />
            </Form.Group>

            <Form.Group controlId="formPhoneNumber">
              <Form.Label>Phone Number</Form.Label>
              <Form.Control type="text" name="phone" />
            </Form.Group>

            <Form.Group controlId="formBasicPassword">
              <Form.Label>Message</Form.Label>
              <Form.Control as="textarea" type="text" name="message" />
            </Form.Group>

            <Button variant="primary" type="submit">
              Submit
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>
  </Layout>
)
