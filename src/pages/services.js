import React from "react"
import { graphql } from "gatsby"
import SEO from "../components/seo"
import Layout from "../components/layout"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"
import Container from "react-bootstrap/Container"
import RowDisplay from "../components/row-display"
import Fade from "react-reveal/Fade"

const ServicesPage = ({ data }) => (
  <Layout>
    <SEO title="Services" />
    <Row id="hero" className="motion-bg text-center">
      <Col className="hero-overlay">
        <div className="hero-text">
          <Fade>
            <h1 className="hero-title">Services</h1>
          </Fade>
        </div>
      </Col>
    </Row>
    <Container className="content services-page">
      {/* Main Content */}
      <Row className="main-content justify-content-center">
        <Col>
          <p>
            Our goal is to ensure that you get the services that will best match
            your business objectives.
          </p>
        </Col>
      </Row>
      <RowDisplay items={data.allMarkdownRemark.edges} itemsPerRow="2" />
    </Container>
  </Layout>
)

export const query = graphql`
  query ServicesQuery {
    allMarkdownRemark(filter: { fileAbsolutePath: { glob: "**/details/**" } }) {
      edges {
        node {
          frontmatter {
            title
            learnMoreLink
            learnMoreLinkText
            icon {
              publicURL
            }
          }
          html
        }
      }
    }
  }
`
export default ServicesPage
