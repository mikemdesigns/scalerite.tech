---
title: Cybersecurity
slug: cybersecurity
---

## Cybersecurity

Every month, there is breaking news about the latest data breach or a new form of cyberattack. You have probably seen headlines like:
• "Meet the new ransomware that knows where you live"
• "Spear-phishing campaign exploiting Windows zero-day vulnerability hits retail, hospitality industries"
• "Cyber attacks on small businesses on the rise"

As these headlines attest, ransomware, phishing, and other types of cyberattacks are increasing in number and sophistication. To avoid becoming a victim, you must take measures to protect your company, even if it is small. No business is too small to go unnoticed by cybercriminals. The fact is that cybercriminals like to attack small companies because those businesses often do not have the expertise or resources to fend them off.

You probably are running anti-malware software already in your business, realizing the essential role it plays in detecting and blocking known ransomware, viruses, and other types of malware. However, that is only one of several measures you need to take to protect your company against cyberattacks. Other important measures include reducing known security vulnerabilities, educating your employees, and preparing for the worst-case scenario.

## Reducing Security Vulnerabilities

You can make it much harder for cybercriminals to attack your IT systems by addressing vulnerabilities that cybercriminals tend to exploit. Here are a few starting points:

## Update Your Software Regularly

Cybercriminals like to target operating system software and applications that have known security vulnerabilities. These vulnerabilities provide a crack that cybercriminals can slip through in order to access your computer systems and install malicious code. Updating your software regularly with newly released patches eliminates known vulnerabilities, thereby reducing the number of exploitable entry points into your computer systems.

## Update Your Firmware

Computers, printers, routers, and other hardware devices include firmware, which is software that gives a device its functionality. Just like software, firmware can have vulnerabilities that cybercriminals exploit. So, it is important to patch your devices' firmware whenever the device manufacturers release an update.

## Upgrade Your Software When Necessary

At some point in time, software vendors stop supporting older operating system software and applications. This means that they do not provide any security updates. Cybercriminals keep track of when versions of popular applications reach their end of support. When that day arrives, cybercriminals intentionally launch new attacks that target the unsupported software. Sometimes, they stockpile malware until the end-of-support date and then set it loose. As a result, your business is much more vulnerable to cyberattacks if you are running software that is no longer supported by the vendor.

Our team can conduct a vulnerability analysis to identify security issues that are leaving your business susceptible to cyberattacks. Once identified, we can work with you to address those vulnerabilities and reduce your risk.

## Educate Employees

Your employees can establish an important line of defense against cybercrime. By educating employees on how cybercriminals carry out cyberattacks, employees can spot these attacks rather than fall victim to them. Phishing, spear phishing, and social engineering should be at the top of your list of topics to cover.

## Phishing and Spear Phishing

Despite being around for years, phishing emails are still being used by cybercriminals to obtain login credentials and other sensitive information, which they then use to steal money and data from businesses. Although people are now more aware of phishing, the attacks are still effective because of the growing sophistication of the emails.

The emails used to be easy to spot, as they often contained numerous misspellings and grammatical errors and spun fantastic tales about how you won the lottery or how a Nigerian prince needs your help. These days, cybercriminals are increasingly posing as legitimate companies, creating emails that look almost identical to real ones sent by those organizations. Plus, cybercriminals sometimes personalize the email to the point where it includes your name and other information about you—a tactic referred to as spear phishing.

Despite being more sophisticated, there are elements that indicate an email might be a phishing or spear phishing attack. Train your employees to look for elements such as:

• A deceptive email address in the "From" field. At first glance, the email address might seem legitimate. For instance, cybercriminals might send out an email message using the address "account-update@amazan.com" instead of the real "account-update@amazon.com" address.
• A request to update or verify information. Cybercriminals like to get sensitive information by posing as a popular legitimate financial institution (e.g., a bank) and asking you to update or verify your information.
• A sense of urgency. A common tactic in a phishing or spear phishing scam is to create a sense of urgency. The cybercriminals first let you know about a problem that requires your attention. Then, they let you know that there will be unfortunate consequences if you do not take action quickly.
• A deceptive URL. A deceptive URL is one in which the actual URL does not match the displayed linked text or web address. For example, the displayed text might specify a legitimate bank name ("Chase") or bank web address ("www.chase.com"), but when you hover your cursor over it (without clicking it), you might discover that the actual URL leads to a website in a foreign country known for cyber attacks.
• An attachment. Cybercriminals sometimes use email attachments to install malware on computers. Many different types of files can contain malicious code, including PDF files and Microsoft Word documents.

When discussing how to spot phishing and spear phishing attacks with employees, be sure to stress the risks associated with clicking an email link or opening an email attachment, especially if the email is from an unknown source. You also need to let employees know what they should do if they receive a suspicious email (e.g., simply delete it, notify someone about it).

## Social Engineering

Cybercriminals sometimes try to con employees into giving them the information they need to access businesses' computer systems or accounts. This is referred to as social engineering. Hackers like to use social engineering attacks because exploiting human behavior is usually easier than hacking security and computer systems.

While social engineering attacks typically occur via email (a.k.a. spear phishing emails), they can also occur over the phone and in person. The cybercriminals often masquerade as employees, but they also might pretend to be suppliers, customers, or even trusted outside authority figures (e.g., firefighters, auditors).

To get into character, cybercriminals usually learn your business's lingo. When cybercriminals use the terms that employees are accustomed to hearing, the employees are more apt to believe the cybercriminals and do what they ask.

Besides learning the business lingo, cybercriminals sometimes search the Internet for information that can help them in their impersonations. Without realizing it, many people provide a lot of information about their professional and personal lives on LinkedIn, Facebook, and other social media sites.

When discussing social engineering with your employees, stress the importance of being careful about what they post on social media sites. It might become fodder for a sophisticated spear phishing attack. Or, it might provide cybercriminals with the information needed to hack online accounts. For example, if an employee posts pictures and stories about her favorite cat, cybercriminals might try using the cat's name as a password or the answer to the security question "What is the name of your favorite pet?" With some online accounts, all it takes to reset a password is an email address and the correct answer to a security question. If cybercriminals are able to reset an account's password, they gain full access to that account.

Our team can share their vast knowledge about cyberattacks with your employees. Armed with this information, your employees can present a formidable line of defense against cyberattacks.

## Prepare for the Worst-Case Scenario

Cybercriminals are constantly devising new ways to attack businesses, so despite your best efforts, your business might become the latest cyberattack victim. This is yet another important reason why you need a data backup strategy.

Although having backup copies of your data and systems will not prevent a cyberattack, it can mitigate the effects of one. For example, if your business becomes the victim of a ransomware attack, you will not have to pay the ransom to get your data back.

We can help you develop a data backup strategy and test it to make sure that your information can be restored in case your company is attacked.

## Protect Your Business

Cybercriminals are constantly releasing new malware programs or variants of existing ones. As a result, relying solely on anti-malware software to protect your business is risky, as it takes a while for the vendors to update their anti-malware software to defend against the new programs and new strains.

After conducting an in-depth security assessment, our security experts can recommend other measures you can take to protect your business from cybercriminals. We can also help train your employees so that they can spot cyberattacks rather than fall victim to them.

**Contact us today to make sure your company has the proper safeguards in place. Waiting until tomorrow might prove to be too late.**
