---
title: Help Desk and Onsite Support
slug: help-desk-onsite-support
---

While many IT services can be provided behind the scenes to avoid disrupting your staff's workday, there are times when you need an IT professional on hand. This becomes particularly evident when employees need assistance and systems need repairs.

## Solving Employees' IT Problems

When your employees have IT problems or questions, who do they contact? Employees often turn to their managers, especially in small and midsize businesses. The managers then need to track down someone who can help, which takes them away from their responsibilities. Meanwhile, the employees become frustrated, as they wait for assistance to arrive.

Worse yet, an employee might turn to a coworker for help. If that person gives the wrong advice, a minor issue could escalate into a major problem. When an employee is suspicious of an email attachment but a coworker says he thinks it is legitimate, the employee could end up opening the attachment and have his computer infected with malware, which can potentially infect your entire network.

Having a reliable help desk can eliminate frustration and lost productivity. Our experienced technicians can assist your team with technical problems so they can get back on track quickly.

Help desk requests and the technicians that handle them are categorized into support levels. The higher the support level, the more difficult the problem is to solve—and the greater the IT expertise needed to resolve them. Help desks can have up to four support levels:

## Level 1

This level of support is typically available 24x7. When employees contact the help desk, the level 1 technicians gather and analyze information about the employees' problems to determine the best way to solve them. Common issues that fall into this level of support include resetting passwords, resolving basic network connectivity issues, and troubleshooting email problems. If the technicians cannot solve a problem, they reclassify it as a level 2 issue.

## Level 2

Level 2 technicians handle more challenging problems, such as troubleshooting software and computer meltdowns. After an investigation, the technicians will determine whether it is a known or new issue. When it is a known issue, they look for solutions in the help desk database. If it is a new issue, they might try to resolve the problem on their own or they might reclassify it as a level 3 issue.

## Level 3

Problems escalated to level 3 are complicated and thus difficult to solve. The problems often lie within the IT infrastructure. Sometimes, level 3 support is handled by networking specialists.

## Level 4

Not all help desks provide level 4 support. This level of support is reserved for extremely complex problems involving multiple vendors, products, or tools outside of the organization.

There is no doubt that having a help desk is advantageous. However, maintaining one in-house is expensive and out of reach for most small to midsize businesses. When that is the case, a cost-effective solution is to use a managed help desk.

We can provide you with a professionally staffed IT help desk that will provide quick and courteous assistance to your employees when they need it.

## Providing Onsite Support

Some computer and network management tasks need to be performed in person. For example, replacing a failing hard drive and setting up a wireless router requires the human touch.

Having an IT expert onsite is also helpful when conducting vulnerability assessments, educating employees about security threats, and solving complex IT problems. Our support services can include on-site help with a friendly, personal touch.

## More Than Just a Matter of Convenience

Having IT professionals readily available is important in ways that go beyond just being convenient. Waiting for someone to resolve problems, answer questions, or provide onsite support leads to lost productivity, which hurts profitability. It can also lead to frustrated employees, who might try to fix a glitch themselves and inadvertently turn a minor issue into a major problem.

**With our help desk and onsite support services, you do not have to wait. We will be there when you need us.**
